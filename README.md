

1. Install [Node](https://nodejs.org/en/).

2. Install Yarn:  
```
npm install -g yarn
```
3. Use [Git](https://git-scm.com/downloads) to clone the necessary repositories.

# frontend-vue

## Project setup
```
yarn install
```

## Compiles and hot-reloads for development
```
yarn serve
```

You can create _.env.development.local_ to override **VUE_APP_API_URL**.

# eupronet-backend

### Install [XAMPP](https://www.apachefriends.org/index.html) if you want to host a local version of the database (get DB structure from [/infrastructure/mariadb-stack/](https://gitlab.com/Deri-Miksa/eupronet/infrastructure/-/blob/master/mariadb-stack/template.sql))

## Project setup
```
yarn install
```
Install Nodemon:
```
yarn global add nodemon
```
(you might need to add the yarn package install location to PATH: use the command *yarn global bin* to get the global installation folder )

Create the *.env* file based on the [backend *.env* example](https://gitlab.com/Deri-Miksa/eupronet/infrastructure/-/blob/master/backend-stack/.env.example)

## Compiles and hot-reloads for development
```
yarn start
```

# Recommended VS Code Extensions
- Vetur
- PlatformIO IDE
- REST Client
- SQLTools

# Other recommended tools
- [GitKraken](https://www.gitkraken.com)
- [Postman](https://www.postman.com)
- [Vue.js devtools](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd?hl=en)

