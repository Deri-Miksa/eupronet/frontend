import { APIResponse, FactoryState } from "@/model";
import store from "@/store/index";
import axios, { AxiosRequestConfig } from "axios";
import { io } from "socket.io-client";
import { useState } from "vuex-composition-helpers";

let token = useState(store, ["token"]).token;

export async function httpGet(url: string, options?: AxiosRequestConfig) {
	let res = await axios.get(url,
	{ headers: { Authorization: token.value }, ...options });

	let data = res.data as APIResponse;

	if (res.status == 200 && data.status == "ok") {
		return data;
	} else {
		console.error({ res, data });
	}

	return undefined;
}

export async function httpPost(url: string, data?: any, options?: AxiosRequestConfig) {
	let res = await axios.post(url, data, 
		{ headers: { Authorization: token.value }, ...options });

	let resData = res.data as APIResponse;

	if (res.status == 200 && resData.status == "ok") {
		return resData;
	} else {
		console.error({ res, data });
	}

	return undefined;
}

export function setupWS() {
	let socket = io(process.env.VUE_APP_API_URL ?? "");
	socket.on("telemetry", function (msg: FactoryState) {
		store.commit("set_assembly_line_state", msg);
	});
	
	store.commit("set_socket", socket);
}