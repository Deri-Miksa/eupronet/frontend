import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

import { httpGet } from "@/utils/connection"
import { ColorReading, FactoryState, Order } from '@/model'
import { io, Socket } from 'socket.io-client'

Vue.use(Vuex)
const vuexLocal = new VuexPersistence({
	storage: window.localStorage,
	reducer: ({ token, username, dark }: any) => {
		return { token, username, dark };
	}
})

export default new Vuex.Store({
	state: {
		token: "",
		username: "",
		dark: false,
		permission: -1,
		assemblyLineState: {
			colorReadings: [],
			RFIDColorPairs: {},
			activeOrders: [],
			finishedOrders: []
		} as FactoryState,
		socket: undefined as Socket | undefined
	},
	mutations: {
		set_theme(state, dark) {
			state.dark = dark
		},
		set_token(state, token) {
			state.token = token
		},
		set_username(state, username) {
			state.username = username
		},
		set_permission(state, permission) {
			state.permission = permission ?? -1;
		},
		set_assembly_line_state({assemblyLineState}, newState: FactoryState) {
			assemblyLineState.colorReadings = newState.colorReadings.map(x => new ColorReading(x.color))
			assemblyLineState.RFIDColorPairs = newState.RFIDColorPairs;
			assemblyLineState.activeOrders = newState.activeOrders.map(x => new Order(x.id, x.orderedItems, x.assignedChips, x.date, x.userid));
			assemblyLineState.finishedOrders = newState.finishedOrders.map(x => new Order(x.id, x.orderedItems, x.assignedChips, x.date, x.userid));
		},
		set_socket(state, socket) {
			state.socket = socket;
		},
	},
	actions: {
		set_theme({commit}, { dark, vm }) {
			commit('set_theme', dark)
			vm.$vuetify.theme.dark = dark
		},
		set_token({commit, dispatch}, token) {
			commit("set_token", token);
			dispatch("get_permission");
		},
		async get_permission({commit}) {
			let data = await httpGet(`${process.env.VUE_APP_API_URL}/permissionlevel`);
			commit("set_permission", data?.permission);
		},
		logout({dispatch}){
			dispatch("set_token", "")
		}
	},
	getters: {
		loggedIn: ({token, username}) => {
			return token != "" && username != ""
		},
		socket: ({socket}) => {
			return socket;
		}
	},
	modules: {
	},
	plugins: [vuexLocal.plugin]
})
