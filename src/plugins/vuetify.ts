import Vue from 'vue';
import Vuetify from 'vuetify/lib';
// import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify, {
    iconfont: 'fa'
});

export default new Vuetify({
    theme: {
        options: {
            customProperties: true,
        },
        themes: {
            dark: {
                primary: "#a0b9d6", // #E53935
                secondary: "#1f1f1f", // #FFCDD2
                accent: "#24325e", // #3F51B5
				discord: "#5865F2",
				gitlab: "#fc6d27",
				github: "#ffffff"
            },
            light: {
                primary: "#24325e",
                secondary: "#1f1f1f",
                accent: "#24325e",
                info: '#FFFFFF',
				discord: "#5865F2",
				gitlab: "#fc6d27",
				github: "#000000"
            },
        },
    },


});
