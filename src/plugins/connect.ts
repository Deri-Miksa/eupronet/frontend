import { connect, NatsConnection } from "nats.ws";
import dotenv from "dotenv";
import { useState } from "vuex-composition-helpers"
import store from "@/store/index"
import { httpGet } from "@/utils/connection";

dotenv.config();

let username: string | undefined, password: string | undefined;
let hostname = process.env.VUE_APP_API_URL ?? "";
let elevated = false;

async function getCredentials() {
	if(username && password && elevated) return;

	username = process.env.VUE_APP_NATS_user
	password = process.env.VUE_APP_NATS_password

	if(username && password && !store.getters['loggedIn']) return;

	let permissionlevel = (await httpGet(`${hostname}/permissionlevel`))?.permission;

	if(permissionlevel >= 7){
		let response = await httpGet(`${hostname}/nats/credentials`);

		username = response?.username;
		password = response?.password
		elevated = true;
	}


}

async function natsConnect(user: string, pass: string) {
	try {
		if (!process.env.VUE_APP_NATS_server) return;

		const nc = await connect({
			servers: [process.env.VUE_APP_NATS_server],
			//port: Number(443),
			maxReconnectAttempts: -1,
			user,
			pass
		});

		console.log(`Successfully connected to ${nc.getServer()}!`);
		return nc;
	} catch (err) {
		console.log(`Failed connecting to nats server: ${err}`);
		console.log(err);
		return undefined;
	}
}

let nc: NatsConnection | undefined;
export async function getConnection() {

	await getCredentials();
	// console.log(username)
	if (!username || !password){
		console.error();
		return undefined;
	}

	if (!nc || nc.isClosed())
		nc = await natsConnect(username, password);

	return nc;
}