import { httpGet } from '@/utils/connection'

import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/index'
const Home    = () => import('../views/Home.vue')
const User    = () => import('../views/User.vue')
const Admin   = () => import('../views/Admin.vue')
const Store   = () => import('../views/Store.vue')
const Metrics = () => import('../views/Metrics.vue')
const Register= () => import('../views/Register.vue')
const Login   = () => import('../views/Login.vue')
const Orders   = () => import('../views/Orders.vue')
const Dashboard   = () => import('@/views/Dashboard.vue')

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Homepage',
    component: Home
  },
  {
    path: '/store',
    name: 'Store',
    component: Store,
    meta: {
      authRequired: true,
	  permission: 0
    }
  },
  {
    path: '/metrics',
    name: 'Metrics',
    component: Metrics
  },
  {
    path: '/account',
    name: 'User settings',
    component: User,
    meta: {
      authRequired: true,
	  permission: 0
    }
  },
  {
    path: '/orders',
    name: 'Order visualization',
    component: Orders,
    meta: {
      authRequired: true,
	  permission: 0
    }
  },
  {
    path: '/admin',
    name: 'Admin control panel',
    component: Admin
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
    meta: {
      authRequired: true,
	  permission: 7
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach(async (routeTo, routeFrom, next) => {
  // Check if auth is required on this route
  // (including nested routes).
  const authRequired = routeTo.matched.some((route) => route.meta.authRequired);

  // If auth isn't required for the route, just continue.
  if (!authRequired) return next();

  if (store.getters['loggedIn']) {
    // Check the permission of the user
	let response = await httpGet(`${process.env.VUE_APP_API_URL}/permissionlevel`);
	if (!response || response.permission < routeTo.matched[0].meta.permission)
		redirectToLogin();

	next();
  }else{
    redirectToLogin()
  }

  function redirectToLogin() {
    // Pass the original route to the login component
    next({ name: 'Login', query: { redirectFrom: routeTo.fullPath } })
  }
})

export default router
