module.exports = {
  "transpileDependencies": [
    "vuetify", "nats.ws"
  ],
  devServer: {
    disableHostCheck: true
  }
}